package ictgradschool.industry.lab04.ex07;

public class Lecturer {

    // instance variables
    private String name;
    private int staffId;
    private String[] papers;
    private boolean onLeave;
    
    public Lecturer(String name, int staffId, String[] papers, boolean onLeave) {
        // TODO Complete this constructor method
        this.name = name;
        this.staffId = staffId;
        this.papers = papers;
        this.onLeave = onLeave;
    }
    
    // TODO Insert getName() method here

    public String getName() {
        return name;
    }
    
    // TODO Insert setName() method here

    public void setName(String newName) {
        this.name = newName; //note here, this.name is same as just name so either are acceptable
    }
    
    // TODO Insert getStaffId() method here

    public int getStaffId() {
        return staffId;
    }

    // TODO Insert setStaffId() method here

    public void setStaffId(int ID) {
        staffId = ID;
    }
    
    // TODO Insert getPapers() method here

    public String[] getPapers () {
        return papers;
    }
    
    // TODO Insert setPapers() method here

    public void setPapers(String[] P){
        papers = P;
    }

    // TODO Insert isOnLeave() method here

    public boolean isOnLeave() {
        return onLeave;
    }

    // TODO Insert setOnLeave() method here

    public void setOnLeave(boolean OL) {
        onLeave = OL;
    }
    
    // TODO Insert toString() method here

    public String toString() {
        return new String ("id:" + staffId + " " + name + "is teaching " + papers.length + "papers." );
    }
    
    // TODO Insert teachesMorePapersThan() method here

    public Boolean teachesMorePapersThan(Lecturer other) {
        if (this.papers.length > other.papers.length) {
            return true;}
        else {return false;}
    }
}